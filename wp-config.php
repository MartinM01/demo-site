<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dxdemose_wp629' );

/** MySQL database username */
define( 'DB_USER', 'dxdemose_wp629' );

/** MySQL database password */
define( 'DB_PASSWORD', 'upJ8@9S8)0' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'thkkus7jkr0ypt73xo6f4zf5ypnzqbfiqighma4wtajhjk90pshqwf7igvwb5i59' );
define( 'SECURE_AUTH_KEY',  'vjbksdco1ennyevf82o8xbevppyzrykoobmv5eteceejmflrji2skql20skhetpw' );
define( 'LOGGED_IN_KEY',    'twxnwnhfeykijgjo8cuttfaqfkuguoyj4tp1vsmqicgsytskycgf1mkwpgpbi9sa' );
define( 'NONCE_KEY',        '3minstai2m4xzqtc6mozanv38cuhhhuroikwxmoiwlwhgfc5lgiij3jesscv9ozi' );
define( 'AUTH_SALT',        'ussdi7e6thaas3e7clg96bqxmkww7dwldwoo3wh24fqqskq8mtgegdcfxuos8ysf' );
define( 'SECURE_AUTH_SALT', 'otqfqwln5vhcn727fjg3l66t6tm4tqiwztg94rctqpl9jv2h5inx242bryvvn1na' );
define( 'LOGGED_IN_SALT',   'if82biruojwb0nknmzb0jigvxrjasmxnmydqevztsbz3rscdcwmbpv5pqczxuy6u' );
define( 'NONCE_SALT',       'c6uqhbugzgqclvo6mrw6v0qph4k11n2qrlitopnzfn1c0ravzv1tkxdkxvcqjp7t' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpv3_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
